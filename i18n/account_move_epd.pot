# Translation of Odoo Server.
# This file contains the translation of the following modules:
# 	* account_move_epd
#
msgid ""
msgstr ""
"Project-Id-Version: Odoo Server 13.0+e-20200629\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-08-07 23:26+0000\n"
"PO-Revision-Date: 2020-08-07 23:26+0000\n"
"Last-Translator: \n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Plural-Forms: \n"

#. module: account_move_epd
#: model_terms:ir.ui.view,arch_db:account_move_epd.account_move_view_search
msgid "Expected 30 days"
msgstr ""

#. module: account_move_epd
#: model_terms:ir.ui.view,arch_db:account_move_epd.account_move_view_search
msgid "Expected 60 days"
msgstr ""

#. module: account_move_epd
#: model_terms:ir.ui.view,arch_db:account_move_epd.account_move_view_search
msgid "Expected 90 days"
msgstr ""

#. module: account_move_epd
#: model:ir.model.fields,field_description:account_move_epd.field_account_move__expected_payment_date
msgid "Expected Payment Date"
msgstr ""

#. module: account_move_epd
#: model_terms:ir.ui.view,arch_db:account_move_epd.account_move_view_search
msgid "Expected Payment date"
msgstr ""

#. module: account_move_epd
#: model_terms:ir.ui.view,arch_db:account_move_epd.account_move_view_search
msgid "Expected more than 90 days"
msgstr ""

#. module: account_move_epd
#: model_terms:ir.ui.view,arch_db:account_move_epd.account_move_view_search
msgid "Expire date"
msgstr ""

#. module: account_move_epd
#: model_terms:ir.ui.view,arch_db:account_move_epd.account_move_view_search
msgid "Expired 30 days"
msgstr ""

#. module: account_move_epd
#: model_terms:ir.ui.view,arch_db:account_move_epd.account_move_view_search
msgid "Expired 60 days"
msgstr ""

#. module: account_move_epd
#: model_terms:ir.ui.view,arch_db:account_move_epd.account_move_view_search
msgid "Expired 90 days"
msgstr ""

#. module: account_move_epd
#: model_terms:ir.ui.view,arch_db:account_move_epd.account_move_view_search
msgid "Expired more than 90 days"
msgstr ""

#. module: account_move_epd
#: model:ir.model,name:account_move_epd.model_account_move
msgid "Journal Entries"
msgstr ""
