# -*- coding: utf-8 -*-
from odoo import _, api, fields, models
from datetime import datetime


class AccountMove(models.Model):
    _inherit = 'account.move'

    expected_payment_date = fields.Date(
    )
